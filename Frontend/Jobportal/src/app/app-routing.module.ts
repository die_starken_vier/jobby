import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from "./app.component";
import {AnzeigeAnschauenComponent} from "./Seiten/anzeigen/anzeige-anschauen/anzeige-anschauen.component";
import {ContentComponent} from "./Seiten/content/content.component";

const routes: Routes = [
  { path: '', redirectTo: '/app', pathMatch: 'full' },
  { path: 'app', component: ContentComponent },
  { path: 'anzeige/:id', component: AnzeigeAnschauenComponent },
  { path: '**', redirectTo: '/app', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {ContentManagementService} from "../../Service/CONTENT/content-management.service";
import {Content} from "../../Service/CONTENT/content.enum";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor(private contentService: ContentManagementService) { }

  ngOnInit(): void {
  }

  anzeigen(): void {
    this.contentService.content = Content.ANZEIGEN;
  }
}

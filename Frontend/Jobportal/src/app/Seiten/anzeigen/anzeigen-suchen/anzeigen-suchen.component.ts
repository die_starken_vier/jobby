import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ContentManagementService} from "../../../Service/CONTENT/content-management.service";
import {Anzeige} from "../../../Model/anzeige";
import {AnzeigeService} from "../../../Service/ANZEIGE/anzeige.service";
import {Router} from "@angular/router";
import {Content} from "../../../Service/CONTENT/content.enum";

@Component({
  selector: 'app-anzeigen-suchen',
  templateUrl: './anzeigen-suchen.component.html',
  styleUrls: ['./anzeigen-suchen.component.scss']
})
export class AnzeigenSuchenComponent implements OnInit {

  searchForm: FormGroup;
  anzeige: Anzeige[] = [];

  constructor(private fb: FormBuilder, public contentService: ContentManagementService, private anzeigeService: AnzeigeService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.searchForm = this.fb.group({
      search: [""]
    });

    this.ladenAnzeigen('');
  }

  ladenAnzeigen(term: string): void {
    this.anzeigeService.anzeigenSuchen(term).subscribe((a: Anzeige[]) => {
      this.anzeige = a;
    });
  }

  onSubmit(){
    this.ladenAnzeigen(this.searchForm.value.search);
  }

  anzeigen(id: string): void {
    this.router.navigate(['anzeige', id]);
  }

}

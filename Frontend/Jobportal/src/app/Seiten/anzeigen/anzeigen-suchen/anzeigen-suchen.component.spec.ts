import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnzeigenSuchenComponent } from './anzeigen-suchen.component';

describe('AnzeigenSuchenComponent', () => {
  let component: AnzeigenSuchenComponent;
  let fixture: ComponentFixture<AnzeigenSuchenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnzeigenSuchenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnzeigenSuchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

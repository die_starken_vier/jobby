import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnzeigeAnschauenComponent } from './anzeige-anschauen.component';

describe('AnzeigeAnschauenComponent', () => {
  let component: AnzeigeAnschauenComponent;
  let fixture: ComponentFixture<AnzeigeAnschauenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnzeigeAnschauenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnzeigeAnschauenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {AnzeigeService} from "../../../Service/ANZEIGE/anzeige.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Anzeige} from "../../../Model/anzeige";
import {Location} from "@angular/common";
import {ContentManagementService} from "../../../Service/CONTENT/content-management.service";
import {Content} from "../../../Service/CONTENT/content.enum";

@Component({
  selector: 'app-anzeige-anschauen',
  templateUrl: './anzeige-anschauen.component.html',
  styleUrls: ['./anzeige-anschauen.component.scss']
})
export class AnzeigeAnschauenComponent implements OnInit {

  titel: string = "";
  content: string = "";

  constructor(private anzeigeService: AnzeigeService, private route: ActivatedRoute,
              private loc: Location, public contentService: ContentManagementService,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id)
    this.anzeigeService.anzeigeLaden(id).subscribe((a: Anzeige) => {
      this.titel = a.titel;
      this.content = a.content;
      console.log(a);
    });
  }

  zurueck(): void {
    this.loc.back();
  }

  bewerben(): void {
    this.contentService.anzeigeID = this.route.snapshot.paramMap.get('id');
    this.contentService.content = Content.BEWERBEN;
    this.router.navigate(['app']);
  }
}

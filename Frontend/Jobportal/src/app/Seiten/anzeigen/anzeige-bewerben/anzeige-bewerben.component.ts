import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AnzeigeService} from "../../../Service/ANZEIGE/anzeige.service";
import {ContentManagementService} from "../../../Service/CONTENT/content-management.service";
import {Content} from "../../../Service/CONTENT/content.enum";

@Component({
  selector: 'app-anzeige-bewerben',
  templateUrl: './anzeige-bewerben.component.html',
  styleUrls: ['./anzeige-bewerben.component.scss']
})
export class AnzeigeBewerbenComponent implements OnInit {
  bewerbungForm: FormGroup;

  constructor(private fb:FormBuilder, private anzeige: AnzeigeService, private content: ContentManagementService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.bewerbungForm = this.fb.group({
      anschreiben: ["", [Validators.minLength(1), Validators.required]]
    });
  }

  onSubmit(){
    if (this.bewerbungForm.invalid)
      return;

    this.anzeige.bewerben(this.content.anzeigeID, this.bewerbungForm.value.anschreiben);
    this.content.content = Content.ANZEIGEN;
  }

}

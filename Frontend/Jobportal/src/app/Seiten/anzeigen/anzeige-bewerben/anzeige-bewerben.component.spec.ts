import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnzeigeBewerbenComponent } from './anzeige-bewerben.component';

describe('AnzeigeBewerbenComponent', () => {
  let component: AnzeigeBewerbenComponent;
  let fixture: ComponentFixture<AnzeigeBewerbenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnzeigeBewerbenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnzeigeBewerbenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

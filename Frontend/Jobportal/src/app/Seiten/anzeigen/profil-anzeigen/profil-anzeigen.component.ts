import {Component, OnInit} from '@angular/core';
import {ProfilContentService} from "../../../Service/CONTENT/profil-content.service";
import {ProfilSeiten} from "../../profil/profil-sidebar/profil-seiten.enum";
import {ContentManagementService} from "../../../Service/CONTENT/content-management.service";
import {Bewerbung} from "../../../Model/bewerbung";
import {AnzeigeService} from "../../../Service/ANZEIGE/anzeige.service";
import {AuthService} from "../../../Service/AUTH/auth.service";

@Component({
  selector: 'app-profil-anzeigen',
  templateUrl: './profil-anzeigen.component.html',
  styleUrls: ['./profil-anzeigen.component.scss']
})
export class ProfilAnzeigenComponent implements OnInit {

  bewerbungen: Bewerbung[] = [];
  istUnternehmen = false;

  constructor(private profilContentService: ProfilContentService, public contentService: ContentManagementService,
              private anzeige: AnzeigeService, private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getProfile().subscribe((p: {name: string, nutzer_typ: number}) => {
          if (p.nutzer_typ === 1) {
            this.istUnternehmen = true;
          } else {
            this.anzeige.getEigeneBewerbungen().subscribe((b: Bewerbung[]) => {
              this.bewerbungen = b;
            })
          }
      });
  }

  erstellen(): void {
    this.profilContentService.selected = ProfilSeiten.anzeige_erstellen;
  }
}

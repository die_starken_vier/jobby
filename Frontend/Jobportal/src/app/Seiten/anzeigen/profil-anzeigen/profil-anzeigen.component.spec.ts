import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilAnzeigenComponent } from './profil-anzeigen.component';

describe('ProfilAnzeigenComponent', () => {
  let component: ProfilAnzeigenComponent;
  let fixture: ComponentFixture<ProfilAnzeigenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilAnzeigenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilAnzeigenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

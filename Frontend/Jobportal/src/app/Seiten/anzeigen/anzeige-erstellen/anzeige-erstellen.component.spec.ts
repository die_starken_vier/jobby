import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnzeigeErstellenComponent } from './anzeige-erstellen.component';

describe('AnzeigeErstellenComponent', () => {
  let component: AnzeigeErstellenComponent;
  let fixture: ComponentFixture<AnzeigeErstellenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnzeigeErstellenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnzeigeErstellenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AnzeigeService} from "../../../Service/ANZEIGE/anzeige.service";
import {ProfilContentService} from "../../../Service/CONTENT/profil-content.service";
import {ProfilSeiten} from "../../profil/profil-sidebar/profil-seiten.enum";

@Component({
  selector: 'app-anzeige-erstellen',
  templateUrl: './anzeige-erstellen.component.html',
  styleUrls: ['./anzeige-erstellen.component.scss']
})
export class AnzeigeErstellenComponent implements OnInit {

  anzeigeForm: FormGroup;

  constructor(private fb: FormBuilder, private anzeigeService: AnzeigeService, private profilContentService: ProfilContentService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.anzeigeForm = this.fb.group({
      titel: ["",[
        Validators.required]],
      info: ["",[
        Validators.required]]
    });
  }

  onSubmit(){

    if(this.anzeigeForm.invalid){
      return;
    }

    this.anzeigeService.anzeigeErstellen(this.anzeigeForm.value.titel, this.anzeigeForm.value.info);
    this.profilContentService.selected = ProfilSeiten.anzeigen;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRegistrationFrameComponent } from './login-registration-frame.component';

describe('LoginRegistrationFrameComponent', () => {
  let component: LoginRegistrationFrameComponent;
  let fixture: ComponentFixture<LoginRegistrationFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRegistrationFrameComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRegistrationFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from "../../Service/AUTH/auth.service";
import {ContentManagementService} from "../../Service/CONTENT/content-management.service";
import {Content} from "../../Service/CONTENT/content.enum";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  //Form wird grupiert:
  registrationForm: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService, private contentService: ContentManagementService) {
  }

  //Flag für Felder "Passwort" bzw "Passwort wiederholen"
  hide = true;

  //Flag für checkbox "Firma/Unternehmen/Projekt"
  firmaCheck: boolean = false;

  ngOnInit(): void {
    this.initForm();
  }

  //initiiert Form
  initForm(): void {
    // Wenn Firma gecheckt wird, dann:
    if (this.isFirmaChecked() == true) {
      this.registrationForm = this.fb.group({
        unternehmenName: ["", [
          Validators.required]],
        vorname: ["", [
          Validators.required]],
        nachname: ["", [
          Validators.required]],
        email: ["", [
          Validators.required, Validators.email]],
        passFirst: ["", [
          Validators.required]],
        passWdh: ["", [
          Validators.required]]
      });
    } else {
      // Wenn privat Person gecheckt wird, dann:
      this.registrationForm = this.fb.group({
        vorname: ["", [
          Validators.required]],
        nachname: ["", [
          Validators.required]],
        geburtsdatum: ["", [
          Validators.required]],
        email: ["", [
          Validators.required, Validators.email]],
        passFirst: ["", [
          Validators.required]],
        passWdh: ["", [
          Validators.required]]
      });
    }

  };

  // Überprüft, ob Feld required ist:
  isControlRequired(controlName: string): boolean {
    if (this.registrationForm.controls[controlName].hasError('required')) {
      return true;
    }
  };

  //Überprüft, ob Email Pattern entspricht:
  isControlEmailPattern(controlName: string): boolean {
    if (this.registrationForm.controls[controlName].hasError('email')) {
      return true;
    }
  };

  //Überprüft, ob Passwort genug lang ist:
  isControlPasswordLength(controlName: string): boolean {
    const required = this.registrationForm.controls[controlName].hasError('required');
    if (!required && this.registrationForm.controls[controlName].value.length < 8) {
      return true;
    }
  };

  //Setzt Flag "firmaCheck" auf true/false, entsprechend die Position auf die Seite.
  //Jedesmal, wenn Flag geändert wird, wird Form neu initiiert:
  setFirmaCheck() {
    if (this.firmaCheck == true) {
      this.firmaCheck = false;
      this.initForm();
    } else if (this.firmaCheck == false) {
      this.firmaCheck = true;
      this.initForm();
    }
  }

  //Überprüft, ob flag "firmaCheck" gecheckt(true) oder nicht(false):
  isFirmaChecked(): boolean {
    return this.firmaCheck;
  }

  //Taste "Registrieren" wird betätigt:
  onSubmit() {
    if (this.registrationForm.invalid) {
      return;
    }

    if (this.isFirmaChecked()) {
      const unternehmen = this.registrationForm.get('unternehmenName').value;
      const name = `${this.registrationForm.get('vorname').value} ${this.registrationForm.get('nachname').value}`;
      const mail = this.registrationForm.get('email').value;
      const password = this.registrationForm.get('passFirst').value;

      this.auth.registerUnternehmen(unternehmen, name, mail, password).subscribe();
    } else {
      const name = `${this.registrationForm.get('vorname').value} ${this.registrationForm.get('nachname').value}`;
      const geb = this.registrationForm.get('geburtsdatum').value;
      const mail = this.registrationForm.get('email').value;
      const password = this.registrationForm.get('passFirst').value;

      this.auth.registerPrivat(name, geb, mail, password).subscribe();
    }

    this.contentService.content = Content.HOME;
  }

}

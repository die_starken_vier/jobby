import {Component, Inject, OnInit, Renderer2} from '@angular/core';
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-karte',
  templateUrl: './karte.component.html',
  styleUrls: ['./karte.component.scss']
})
export class KarteComponent implements OnInit {

  constructor(private _renderer2: Renderer2,
  @Inject(DOCUMENT) private _document: Document) { }

  ngOnInit(): void {
    let script = this._renderer2.createElement('script');
    script.type = 'text/javascript';
    script.src = '../../../assets/scripts/karte.js'
    this._renderer2.appendChild(this._document.body, script);
  }

}

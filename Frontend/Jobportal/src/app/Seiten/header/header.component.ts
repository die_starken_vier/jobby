import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Content} from "../../Service/CONTENT/content.enum";
import {ContentManagementService} from "../../Service/CONTENT/content-management.service";
import {AuthService} from "../../Service/AUTH/auth.service";
import {Router} from "@angular/router";
import {ProfilBasic} from "../../Model/profil-basic";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  cnt = Content;

  constructor(private contentService: ContentManagementService, private auth: AuthService, private router: Router) { }

  eingeloggt = this.contentService.isLoggedIn;
  name = '';
  benachrichtigungen = this.contentService.benachrichtigung;

  ngOnInit(): void {
    if (this.auth.getAccessToken()) {
      this.eingeloggt = true;
      this.auth.getProfile().subscribe((u: ProfilBasic) => {
        this.name = u.name;
        return;
      })
    }
  }

  setContent(content: Content) {
    this.contentService.content = content;
    if (!this.router.isActive('app', true)) {
      this.router.navigate(['app']);
    }
  }

  logout() {
    this.auth.logout();
  }
}

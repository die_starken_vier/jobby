import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AccountManagementService} from "../../../Service/ACCOUNT-MANAGEMENT/account-management.service";
import {Session} from "../../../Model/session";

@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.scss']
})
export class AccountManagementComponent implements OnInit {

  passForm: FormGroup;
  mailForm: FormGroup;

  sessions: Session[] = [];

  passResultShow = false;
  passResult = false;

  @ViewChild('neuPassWdh') neuPassWdh: ElementRef;

  constructor(private am: AccountManagementService) {
  }

  ngOnInit(): void {
    this.profilLaden();
  }

  profilLaden(): void {
    this.passForm = new FormGroup({
      altPass: new FormControl('', [Validators.required, Validators.minLength(8)]),
      neuPass: new FormControl('', [Validators.required, Validators.minLength(8)]),
      neuPassWdh: new FormControl('', [Validators.required, Validators.minLength(8)])
    });

    this.mailForm = new FormGroup({
      newMail: new FormControl('', [Validators.required, Validators.email]),
      pass: new FormControl('', [Validators.required])
    });

    this.ladenSessions();
  }

  ladenSessions(): void {
    this.am.getSessions().subscribe((s: Session[]) => {
      s.forEach(sess => {
        this.sessions.push({
          _id: sess._id,
          last_active: sess.last_active,
          ip: sess.ip,
          token: sess.token
        })
      })
    });
  }

  //
  //  SPEICHERN
  //
  async speichernPass(): Promise<void> {
      if (this.passForm.invalid)
        return;

      const success = await this.am.speichernNeuesPass(this.passForm.get('altPass').value, this.passForm.get('neuPass').value);

      this.passForm.reset();
      this.passForm.get('altPass').setErrors(null);
      this.passForm.get('neuPass').setErrors(null);
      this.passForm.get('neuPassWdh').setErrors(null);

      this.passResultShow = true;
      this.passResult = success;
  }

  speichernMail(): void {
    if (this.mailForm.invalid)
      return;

    this.am.speichernNeuMail(this.mailForm.get('newMail').value, this.mailForm.get('pass').value);

    this.mailForm.reset();
    this.mailForm.get('newMail').setErrors(null);
    this.mailForm.get('pass').setErrors(null);
  }

  //
  //  LOGOUT SESSION
  //
  logoutSession(_id: string): void {
    this.am.ausloggenSession(_id);
    this.sessions = this.sessions.filter(s => s._id !== _id);
  }
}

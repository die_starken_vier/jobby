import {Component, OnInit} from '@angular/core';
import {ProfilPrivat} from "../../../Model/profil-privat";
import {ProfilLadenService} from "../../../Service/PROFIL/profil-laden.service";
import {ContentManagementService} from "../../../Service/CONTENT/content-management.service";
import {ProfilSeiten} from "./profil-seiten.enum";
import {ProfilContentService} from "../../../Service/CONTENT/profil-content.service";
import {AuthService} from "../../../Service/AUTH/auth.service";
import {ProfilBasic} from "../../../Model/profil-basic";

@Component({
  selector: 'app-profil-sidebar',
  templateUrl: './profil-sidebar.component.html',
  styleUrls: ['./profil-sidebar.component.scss']
})
export class ProfilSidebarComponent implements OnInit {

  profil: ProfilPrivat = undefined;
  privat: boolean;
  seiten = ProfilSeiten;

  constructor(private profilLaden: ProfilLadenService, private contentManagement: ContentManagementService, public profilContent: ProfilContentService, private auth: AuthService) {

  }

  ngOnInit(): void {
    this.auth.getProfile().subscribe((p: ProfilBasic) => {
      this.privat = p.nutzer_typ === 0;
      return;
    });

  }

  auswahl(ps: ProfilSeiten): void {
    this.profilContent.selected = ps;
  }

}

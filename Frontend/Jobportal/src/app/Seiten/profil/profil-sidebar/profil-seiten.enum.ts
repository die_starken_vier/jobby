export enum ProfilSeiten {
  vorstellungsprofil,
  bewerbungen,
  bewerbungen_gesamt,
  bewerbungen_absagen,
  bewerbungen_zusagen,
  chats,
  meldungen,
  account_management,
  anzeigen,
  bewerbungen_unternehmen_anfragen,
  bewerbungen_unternehmen_auswahl,
  bewerbungen_unternehmen_zusagen,
  bewerbungen_unternehmen_absagen,
  anzeige_erstellen
}

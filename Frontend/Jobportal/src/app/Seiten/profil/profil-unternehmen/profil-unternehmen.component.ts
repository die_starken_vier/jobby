import {Component, OnInit} from '@angular/core';
import {ProfilLadenService} from "../../../Service/PROFIL/profil-laden.service";
import {Kontakt} from "../../../Model/kontakt.enum";
import {FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {ProfilUnternehmen} from "../../../Model/profil-unternehmen";

@Component({
  selector: 'app-profil-unternehmen',
  templateUrl: './profil-unternehmen.component.html',
  styleUrls: ['./profil-unternehmen.component.scss']
})
export class ProfilUnternehmenComponent implements OnInit {

  profil: ProfilUnternehmen = undefined;
  edit = false;
  kontaktList: Kontakt[] = Object.keys(Kontakt).map(key => Kontakt[key]);
  profilForm: FormGroup;

  bild;
  link: string;

  speicherLock = false;

  profilKontaktArray: FormArray = new FormArray([]);
  profilCustomBox: FormArray = new FormArray([]);
  profilAdresse: FormGroup = new FormGroup({});

  constructor(private profilLaden: ProfilLadenService, private fb: FormBuilder) {
  }

  async ngOnInit(): Promise<void> {
    await this.ladenProfil();
  }

  private async ladenProfil() {
    this.profilLaden.unternehmenLaden().subscribe((p: ProfilUnternehmen) => {
      this.profil = p;

      this.initFormArrays();
      this.bild = this.profil.bild;
      this.link = this.profil.homepage;

      this.profilForm = this.fb.group({
        name: new FormControl({value: this.profil.name, disabled: !this.edit}, Validators.required),
        adresse: this.profilAdresse,
        link: new FormControl({value: this.profil.homepage, disabled: !this.edit}),
        kontakt: this.profilKontaktArray,
        customBox: this.profilCustomBox
      });
    });
  }

//
  //  CUSTOM VALIDATORS
  //

  kontaktValidator(form: FormGroup): ValidationErrors {
    const kontaktControl = <FormControl>form.get('kontakt');
    const kontakt = <Kontakt>kontaktControl.value;

    const angabeControl = <FormControl>form.get('angabe');

    if (kontakt === Kontakt.EMAIL) {
      angabeControl.setValidators([Validators.required, Validators.email]);
    }
    if (kontakt === Kontakt.TELEFON) {
      angabeControl.setValidators([Validators.required]);
    }

    return null;
  }

  //
  //  OPEN LINK
  //

  goToLink(): void {
    window.open(this.link, "_blank");
  }

  //
  //  CREATE METHODEN
  //

  initFormArrays(): void {
    if (this.profil.kontaktangaben) {
      this.profil.kontaktangaben.forEach(k => {
        this.addKontakt(k.kontakt, k.angabe);
      });
    }

    if (this.profil.customBox) {
      this.profil.customBox.forEach(cb => {
        this.addCustomBox(cb.titel, cb.content);
      })
    }

    if (this.profil.adresse) {
      this.profilAdresse = new FormGroup({
        strasse: new FormControl({value: this.profil.adresse.strasse, disabled: !this.edit}, Validators.required),
        hausnr: new FormControl({value: this.profil.adresse.nummer, disabled: !this.edit}, Validators.required),
        plz: new FormControl({value: this.profil.adresse.plz, disabled: !this.edit}, Validators.required),
        ort: new FormControl({value: this.profil.adresse.ort, disabled: !this.edit}, Validators.required),
        land: new FormControl({value: this.profil.adresse.land, disabled: !this.edit}, Validators.required),
      })
    } else {
      this.profilAdresse = new FormGroup({
        strasse: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        hausnr: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        plz: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        ort: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        land: new FormControl({value: '', disabled: !this.edit}, Validators.required),
      })
    }
  }

  createKontakt(kontakt: Kontakt, angabe: string): FormGroup {
    return new FormGroup({
      kontakt: new FormControl({value: kontakt, disabled: !this.edit}, Validators.required),
      angabe: new FormControl({value: angabe, disabled: !this.edit},
        [Validators.required])
    }, Validators.compose([this.kontaktValidator]))
  }

  createCustomBox(titel: string, content: string): FormGroup {
    return new FormGroup({
      titel: new FormControl({value: titel, disabled: !this.edit}, Validators.required),
      content: new FormControl({value: content, disabled: !this.edit}, Validators.required)
    });
  }

  //
  //  ADD
  //

  addKontakt(k: Kontakt, a: string): void {
    this.profilKontaktArray.push(this.createKontakt(k, a));
  }

  addCustomBox(titel: string, content: string): void {
    this.profilCustomBox.push(this.createCustomBox(titel, content));
  }

  //
  //  EDIT MODE
  //

  toggleEdit(): void {
    this.edit = !this.edit;

    for (let key in this.profilForm.controls) {
      // Alter soll nicht manuell bearbeitet werden, da dies durch das Geb. Datum errechnet wird
      if (this.edit) {
        this.profilForm.controls[key].enable();
        if (this.speicherLock) this.speicherLock = false;
      } else {
        this.profilForm.controls[key].disable();
        // Speichert die Veränderungen
        this.speichern();
      }
    }
  }

  //
  //  INTERAKT KNÖPFE
  //

  // KONTAKT

  deleteKontaktKnopf(index: number): void {
    this.profilKontaktArray.removeAt(index);
  }

  addKontaktKnopf(): void {
    this.addKontakt(Kontakt.EMAIL, '');
  }

  // CUSTOM BOX

  deleteCustomBoxKnopf(index: number): void {
    this.profilCustomBox.removeAt(index);
  }

  addCustomBoxKnopf(): void {
    this.addCustomBox('', '');
  }

  // BILD

  uploadImage(event): void {
    let file: File = event.target.files[0];

    // Stellt trivial sicher, dass es ein Bild ist
    if (!file.type.toString().startsWith('image'))
      return;

    // TODO: Severseitig die Datei prüfen
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.bild = reader.result;
    }
  }

  //
  //  SUBMIT
  //

  speichern(): void {
    if (this.profilForm.invalid) {
      return;
    }

    if (!this.speicherLock) {
      this.speicherLock = true;
      // TODO: BILD :P
      const p = this.profilForm;

      const profil = <ProfilUnternehmen>{
        adresse: p.get('adresse').value,
        bild: this.bild,
        customBox: p.get('customBox').value,
        kontaktangaben: p.get('kontakt').value,
        name: p.get('name').value,
        homepage: p.get('link').value
      }

      this.link = profil.homepage;

      this.profilLaden.unternehmenSpeichern(profil);

      // Stellt sicher das Edit immer FALSE wird
      this.edit = true;
      this.toggleEdit();
    }
  }

  //
  //  RESET
  //

  async reset(): Promise<void> {

    this.profil = undefined;

    this.profilForm = undefined;

    this.profilKontaktArray = new FormArray([]);
    this.profilCustomBox = new FormArray([]);

    await this.ladenProfil();

    // Stellt sicher das Edit immer zu FALSE getoggled wird
    this.edit = true;
    this.toggleEdit();
  }
}


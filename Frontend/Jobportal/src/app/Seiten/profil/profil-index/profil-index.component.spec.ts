import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilIndexComponent } from './profil-index.component';

describe('ProfilIndexComponent', () => {
  let component: ProfilIndexComponent;
  let fixture: ComponentFixture<ProfilIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilPrivatComponent } from './profil-privat.component';

describe('ProfilPrivatComponent', () => {
  let component: ProfilPrivatComponent;
  let fixture: ComponentFixture<ProfilPrivatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilPrivatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilPrivatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

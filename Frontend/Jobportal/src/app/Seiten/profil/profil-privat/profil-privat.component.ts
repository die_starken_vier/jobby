import {Component, Injectable, OnInit} from '@angular/core';
import {ProfilPrivat} from "../../../Model/profil-privat";
import {ProfilLadenService} from "../../../Service/PROFIL/profil-laden.service";
import {Kontakt} from "../../../Model/kontakt.enum";
import {FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter} from "@angular/material/core";
import {formatDate} from "@angular/common";

export const MY_FORMATS = {
  parse: {dateInput: {month: 'short', year: 'numeric', day: 'numeric'}},
  display: {
    dateInput: 'input',
    monthYearLabel: {year: 'numeric', month: 'short'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'}
  }
};

@Injectable()
class PickDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      return formatDate(date, 'dd.MM.yyyy', this.locale);
    } else {
      return date.toDateString();
    }
  }
}

@Component({
  selector: 'app-profil-privat',
  templateUrl: './profil-privat.component.html',
  styleUrls: ['./profil-privat.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: PickDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ]
})
export class ProfilPrivatComponent implements OnInit {

  profil: ProfilPrivat = undefined;
  edit = false;
  kontaktList: Kontakt[] = Object.keys(Kontakt).map(key => Kontakt[key]);
  profilForm: FormGroup;

  bild: string | ArrayBuffer = "";

  speicherLock = false;

  profilKontaktArray: FormArray = new FormArray([]);
  profilBildungArray: FormArray = new FormArray([]);
  profilBerufArray: FormArray = new FormArray([]);
  profilCustomBox: FormArray = new FormArray([]);
  profilAdresse: FormGroup = new FormGroup({});

  constructor(private profilLaden: ProfilLadenService, private fb: FormBuilder) {
  }

  async ngOnInit(): Promise<void> {
    await this.ladenProfil();
  }

  private ladenProfil() {
    this.profilLaden.privatLaden().subscribe((p: {banned: boolean, geburtsdatum: Date, mail: string, name: string, nutzer_typ: number, vorstellungsprofil: ProfilPrivat}) => {
      // @ts-ignore
      this.profil = p.vorstellungsprofil;

      this.initFormArrays();

      if (this.profil.bild) {
        this.bild = "data:image/jpeg;base64," + this.profil.bild;
      }

      this.profil.alter = this.yearDiff(new Date(p.geburtsdatum), new Date());

      this.profilForm = this.fb.group({
        name: new FormControl({value: p.name, disabled: !this.edit}, Validators.required),
        geb: new FormControl({value: p.geburtsdatum, disabled: !this.edit}, [Validators.required]),
        alter: new FormControl({value: this.profil.alter, disabled: true},
          [Validators.required, Validators.pattern('[0-9]+')]),
        adresse: this.profilAdresse,
        kontakt: this.profilKontaktArray,
        bildung: this.profilBildungArray,
        beruf: this.profilBerufArray,
        kenntnisse: new FormControl({value: this.profil.kenntnisse, disabled: !this.edit}, Validators.required),
        customBox: this.profilCustomBox
      });
    });
  }

//
  //  CUSTOM VALIDATORS
  //

  kontaktValidator(form: FormGroup): ValidationErrors {
    const kontaktControl = <FormControl>form.get('kontakt');
    const kontakt = <Kontakt>kontaktControl.value;

    const angabeControl = <FormControl>form.get('angabe');

    if (kontakt === Kontakt.EMAIL) {
      angabeControl.setValidators([Validators.required, Validators.email]);
    }
    if (kontakt === Kontakt.TELEFON) {
      angabeControl.setValidators([Validators.required]);
    }

    return null;
  }

  //
  //  CREATE METHODEN
  //

  initFormArrays(): void {
    if (this.profil.adresse[0]) {
      this.profilAdresse = new FormGroup({
        strasse: new FormControl({value: this.profil.adresse[0].strasse, disabled: !this.edit}, Validators.required),
        nummer: new FormControl({value: this.profil.adresse[0].hausnr, disabled: !this.edit}, Validators.required),
        plz: new FormControl({value: this.profil.adresse[0].plz, disabled: !this.edit}, Validators.required),
        ort: new FormControl({value: this.profil.adresse[0].ort, disabled: !this.edit}, Validators.required),
        land: new FormControl({value: this.profil.adresse[0].land, disabled: !this.edit}, Validators.required),
      });
    } else {
      this.profilAdresse = new FormGroup({
        strasse: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        nummer: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        plz: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        ort: new FormControl({value: '', disabled: !this.edit}, Validators.required),
        land: new FormControl({value: '', disabled: !this.edit}, Validators.required),
      });
    }

    if (this.profil.kontaktangaben) {
      this.profil.kontaktangaben.forEach(k => {
        this.addKontakt(k.kontaktmoeglichkeit, k.angabe);
      });
    }

    if (this.profil.kontaktangaben) {
      this.profil.bildung.forEach(b => {
        this.addBildung(b.schule, b.von, b.bis, b.abschluss);
      })
    }

    if (this.profil.berufserfahrung) {
      this.profil.berufserfahrung.forEach(b => {
        this.addBeruf(b.firma, b.bezeichnung, b.von, b.bis);
      })
    }

    if (this.profil.customBox) {
      this.profil.customBox.forEach(cb => {
        this.addCustomBox(cb.titel, cb.content);
      })
    }
  }

  createKontakt(kontakt: Kontakt, angabe: string): FormGroup {
    return new FormGroup({
      kontaktmoeglichkeit: new FormControl({value: kontakt, disabled: !this.edit}, Validators.required),
      angabe: new FormControl({value: angabe, disabled: !this.edit},
        [Validators.required])
    })
  }

  createBildung(schule: string, von: Date, bis: Date, abschluss: string): FormGroup {
    return new FormGroup({
      schule: new FormControl({value: schule, disabled: !this.edit}, Validators.required),
      von: new FormControl({value: new Date(von), disabled: !this.edit}, Validators.required),
      bis: new FormControl({value: new Date(bis), disabled: !this.edit}, Validators.required),
      abschluss: new FormControl({value: abschluss, disabled: !this.edit})
    });
  }

  createBeruf(beruf: string, bez: string, von: Date, bis: Date): FormGroup {
    return new FormGroup({
      firma: new FormControl({value: beruf, disabled: !this.edit}, Validators.required),
      bezeichnung: new FormControl({value: bez, disabled: !this.edit}, Validators.required),
      von: new FormControl({value: new Date(von), disabled: !this.edit}, Validators.required),
      bis: new FormControl({value: new Date(bis), disabled: !this.edit}, Validators.required)
    });
  }

  createCustomBox(titel: string, content: string): FormGroup {
    return new FormGroup({
      titel: new FormControl({value: titel, disabled: !this.edit}, Validators.required),
      content: new FormControl({value: content, disabled: !this.edit}, Validators.required)
    });
  }

  //
  //  ADD
  //

  addKontakt(k: Kontakt, a: string): void {
    this.profilKontaktArray.push(this.createKontakt(k, a));
  }

  addBildung(schule: string, von: Date, bis: Date, abschluss: string): void {
    this.profilBildungArray.push(this.createBildung(schule, von, bis, abschluss));
  }

  addBeruf(beruf: string, stelle: string, von: Date, bis: Date): void {
    this.profilBerufArray.push(this.createBeruf(beruf, stelle, von, bis));
  }

  addCustomBox(titel: string, content: string): void {
    this.profilCustomBox.push(this.createCustomBox(titel, content));
  }

  //
  //  EDIT MODE
  //

  toggleEdit(): void {
    this.edit = !this.edit;

    for (let key in this.profilForm.controls) {
      // Alter soll nicht manuell bearbeitet werden, da dies durch das Geb. Datum errechnet wird
      if (this.edit) {
        if (key !== 'alter') this.profilForm.controls[key].enable();
        if (this.speicherLock) this.speicherLock = false;
      } else {
        this.profilForm.controls[key].disable();
        // Speichert die Veränderungen
        this.speichern();
      }
    }
  }

  //
  //  INTERAKT KNÖPFE
  //

  // KONTAKT

  deleteKontaktKnopf(index: number): void {
    this.profilKontaktArray.removeAt(index);
  }

  addKontaktKnopf(): void {
    this.addKontakt(Kontakt.EMAIL, '');
  }

  // BILDUNG

  deleteBildungKnopf(index: number): void {
    this.profilBildungArray.removeAt(index);
  }

  addBildungKnopf(): void {
    this.addBildung('', new Date(), new Date(), '');
  }

  // BERUF

  deleteBerufKnopf(index: number): void {
    this.profilBerufArray.removeAt(index);
  }

  addBerufKnopf(): void {
    this.addBeruf('', '', new Date(), new Date());
  }

  // CUSTOM BOX

  deleteCustomBoxKnopf(index: number): void {
    this.profilCustomBox.removeAt(index);
  }

  addCustomBoxKnopf(): void {
    this.addCustomBox('', '');
  }

  // BILD

  uploadImage(event): void {
    let file: File = event.target.files[0];

    // Stellt trivial sicher, dass es ein Bild ist
    if (!file.type.toString().startsWith('image'))
      return;

    // TODO: Severseitig die Datei prüfen
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.bild = reader.result;
    }
  }

  //
  //  BESONDERE FUNKTIONEN
  //

  yearDiff(date1: Date, date2: Date): number {
    const j1 = date1.getFullYear();
    const m1 = date1.getMonth();
    const d1 = date1.getDay();

    const j2 = date2.getFullYear();
    const m2 = date2.getMonth();
    const d2 = date2.getDay();

    let diff = j1 - j2;

    // Überpüft ob der neue Monat vor dem alten liegt
    if (m1 < m2) {
      // Wenn True -> Jahre unterschied - 1
      diff--;
    } else {
      // Ansonsten schau ob der Monat der gleiche ist
      if (m1 === m2) {
        // Wenn True -> Überpüfe ob der Tag vor dem vom alten ist
        if (d1 < d2) {
          // Wenn true -> Jahre unterschied - 1
          diff--;
        }
      }
    }

    return diff;
  }

  rechnenAlter(): void {
    const geb = <Date>this.profilForm.get('geb').value;
    const heute = new Date();
    const diff = this.yearDiff(heute, geb);
    this.profilForm.get('alter').setValue(diff);
  }

  //
  //  SUBMIT
  //

  speichern(): void {
    if (this.profilForm.invalid) {
      return;
    }

    if (!this.speicherLock) {
      this.speicherLock = true;
      const p = this.profilForm;

      this.profilLaden.privatSpeichern(<ProfilPrivat>{
        adresse: p.get('adresse').value,
        berufserfahrung: p.get('beruf').value,
        bild: this.bild,
        bildung: p.get('bildung').value,
        customBox: p.get('customBox').value,
        geburtsdatum: p.get('geb').value,
        kenntnisse: p.get('kenntnisse').value,
        kontaktangaben: p.get('kontakt').value,
        name: p.get('name').value
      });

      // Stellt sicher das Edit immer FALSE wird
      this.edit = true;
      this.toggleEdit();
    }
  }

  //
  //  RESET
  //

  async reset(): Promise<void> {

    this.profil = undefined;

    this.profilForm = undefined;

    this.profilKontaktArray = new FormArray([]);
    this.profilBildungArray = new FormArray([]);
    this.profilBerufArray = new FormArray([]);
    this.profilCustomBox = new FormArray([]);

    await this.ladenProfil();

    // Stellt sicher das Edit immer zu FALSE getoggled wird
    this.edit = true;
    this.toggleEdit();
  }
}

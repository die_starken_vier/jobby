import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Content} from '../../Service/CONTENT/content.enum';
import {ContentManagementService} from '../../Service/CONTENT/content-management.service';
import {AuthService} from "../../Service/AUTH/auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup ;
  hide = true;

  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void{

    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required, Validators.email]],
      passwort: ['', [
        Validators.required]],
    });
  }

  onSubmit(): void {
    this.auth.login(this.loginForm.get('email').value, this.loginForm.get('passwort').value).subscribe();
  }

}

import { Component, OnInit } from '@angular/core';
import {ContentManagementService} from "../../Service/CONTENT/content-management.service";
import {Content} from "../../Service/CONTENT/content.enum";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  cnt = Content;

  constructor(private contentService: ContentManagementService) { }

  ngOnInit(): void {
  }

  selected(content: Content): boolean {
    return content === this.contentService.content;
  }

}

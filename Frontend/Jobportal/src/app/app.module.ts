import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './Seiten/header/header.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatBadgeModule} from "@angular/material/badge";
import { RegistrationComponent } from './Seiten/registration/registration.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatChipsModule} from "@angular/material/chips";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {MatNativeDateModule} from "@angular/material/core";
import {MatCardModule} from "@angular/material/card";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ApiRequestInterceptor} from "./Service/API/api-request.interceptor";
import { ProfilIndexComponent } from './Seiten/profil/profil-index/profil-index.component';
import { ProfilPrivatComponent } from './Seiten/profil/profil-privat/profil-privat.component';
import { ProfilUnternehmenComponent } from './Seiten/profil/profil-unternehmen/profil-unternehmen.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatExpansionModule} from "@angular/material/expansion";
import { ProfilSidebarComponent } from './Seiten/profil/profil-sidebar/profil-sidebar.component';
import { ContentComponent } from './Seiten/content/content.component';
import { HomepageComponent } from './Seiten/homepage/homepage.component';
import { ProfilAnzeigenComponent } from './Seiten/anzeigen/profil-anzeigen/profil-anzeigen.component';
import { AnzeigenSuchenComponent } from './Seiten/anzeigen/anzeigen-suchen/anzeigen-suchen.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import { AnzeigeErstellenComponent } from './Seiten/anzeigen/anzeige-erstellen/anzeige-erstellen.component';
import { AnzeigeAnschauenComponent } from './Seiten/anzeigen/anzeige-anschauen/anzeige-anschauen.component';
import { AnzeigeBewerbenComponent } from './Seiten/anzeigen/anzeige-bewerben/anzeige-bewerben.component';
import { AccountManagementComponent } from './Seiten/profil/account-management/account-management.component';
import {LoginComponent} from "./Seiten/login/login.component";
import {LoginRegistrationFrameComponent} from "./Seiten/login-registration-frame/login-registration-frame.component";
import { KarteComponent } from './Seiten/karte/karte.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProfilIndexComponent,
    ProfilPrivatComponent,
    ProfilUnternehmenComponent,
    RegistrationComponent,
    ProfilSidebarComponent,
    ContentComponent,
    AccountManagementComponent,
    ContentComponent,
    HomepageComponent,
    ProfilAnzeigenComponent,
    AnzeigenSuchenComponent,
    AnzeigeErstellenComponent,
    AnzeigeAnschauenComponent,
    AnzeigeBewerbenComponent,
    LoginComponent,
    LoginRegistrationFrameComponent,
    KarteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatBadgeModule,
    HttpClientModule,
    MatBadgeModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatChipsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMomentDateModule,
    FormsModule,
    MatNativeDateModule,
    MatCardModule,
    MatButtonToggleModule,
    MatSidenavModule,
    MatExpansionModule,
    MatPaginatorModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ApiRequestInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

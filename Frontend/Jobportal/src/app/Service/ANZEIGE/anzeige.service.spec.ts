import { TestBed } from '@angular/core/testing';

import { AnzeigeService } from './anzeige.service';

describe('AnzeigeService', () => {
  let service: AnzeigeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnzeigeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

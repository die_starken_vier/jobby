import { Injectable } from '@angular/core';
import {ApiRequestService} from "../API/api-request.service";
import {Anzeige} from "../../Model/anzeige";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AnzeigeService {

  constructor(private api: ApiRequestService) { }

   anzeigenSuchen(term: string): Observable<object> {
     return this.api.post('/api/anzeige/suchen', {
      suche: term
    });
  }

  anzeigeLaden(id: string): Observable<object> {
    return this.api.get(`/api/anzeige/${id}`);
  }

  anzeigeErstellen(titel: string, content: string): void {
    this.api.post('/api/anzeige/erstellen', {
      titel: titel,
      content: content
    }, { responseType: 'text' }).subscribe();
  }

  bewerben(anzeigeID: string, anschreiben: string): void {
    this.api.post('/api/anzeige/bewerbung/erstellen', {
      anzeigeID: anzeigeID,
      anschreiben: anschreiben
    }, {responseType: 'text'}).subscribe();
  }

  getEigeneBewerbungen(): Observable<object> {
    return this.api.get('/api/anzeige/bewerbung/me');
  }
}

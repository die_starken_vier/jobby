import { Injectable } from '@angular/core';
import {ApiRequestService} from "../API/api-request.service";
import {Observable} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AccountManagementService {

  constructor(private api: ApiRequestService) { }

  //  Sendet das eingegebene alte Passwort und das neue an das Backend.
  //  Wenn ein Fehlercode zurückkommt, dann war der Prozess nicht erfolgreich
  //  und das neue Passwort wurde nicht gespeichert!
  async speichernNeuesPass(altPass: string, neuPass: string): Promise<boolean> {
    let success = true;

    const r = await this.api.post('/api/user/me/pass/change', {
        pass: altPass,
        newpass: neuPass
      }, {responseType: 'text'}
    ).toPromise().then((res) => {
      success = true;
    }).catch((err: HttpErrorResponse) => {
      success = false;
    });

    return success;
  }

  speichernNeuMail(_mail: string, pass: string): void {
    this.api.post('/api/user/me/mail/change', {
      mail: _mail,
      pass: pass
    },{responseType: 'text'}
    ).subscribe();
  }

  ausloggenSession(id: string): void {
    this.api.post('/api/user/me/sessions/logout', {
      sessionID: id
    },{responseType: 'text'}
    ).subscribe();
  }

  getSessions(): Observable<object> {
    return this.api.get('/api/user/me/sessions');
  }
}

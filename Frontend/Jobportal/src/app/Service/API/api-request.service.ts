import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiRequestService {

  constructor(private web: HttpClient) { }

  get(uri: string, payload: object = {}): Observable<object> {
    return this.web.get(`${environment.apiURL}${uri}`, payload);
  }

  post(uri: string, payload: object = {}, options: object = {}): Observable<object> {
    return this.web.post(`${environment.apiURL}${uri}`, payload, options);
  }
}

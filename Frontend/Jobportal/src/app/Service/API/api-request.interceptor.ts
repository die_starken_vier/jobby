import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpRequest,
  HttpHandler,
  HttpInterceptor
} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {AuthService} from "../AUTH/auth.service";
import {catchError} from "rxjs/operators";

@Injectable()
export class ApiRequestInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {}

  //
  //  Fängt jede Request vom Client ab und fügt, falls vorhanden, den Access Token an
  //  Wenn der Access Token nicht vorhanden ist wird dieser nicht Angehandens
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<any> {
    request = this.addAuthHeader(request);

    return next.handle(request).pipe(catchError((err: HttpErrorResponse) => {

      if (err.status === 401) {
        this.auth.logout().then(() => {
          return EMPTY;
        });
      } else {
        return next.handle(request);
      }

      }));
  }

  private addAuthHeader(request: HttpRequest<unknown>): HttpRequest<unknown> {
    const token = this.auth.getAccessToken();

    if (token) {
      return request.clone({
        setHeaders: {
          'x-access-token': token
        }
      });
    }

    return request;
  }
}

import {Injectable} from '@angular/core';
import {ApiRequestService} from "../API/api-request.service";
import {EMPTY, Observable} from "rxjs";
import {catchError, shareReplay, tap} from "rxjs/operators";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {ContentManagementService} from "../CONTENT/content-management.service";
import {ProfilBasic} from "../../Model/profil-basic";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  name: string;
  isPrivate: boolean;

  constructor(private api: ApiRequestService, private contentService: ContentManagementService) {}

  login(mail: string, password: string): Observable<object> {
    return this.api
      .post('/api/user/login', {mail: mail, password: password}, {observe: 'response', responseType: 'text'})
      .pipe(
        shareReplay(),
        tap((res: HttpResponse<any>) => {
          if (!this.createSession(res)) {
            throw Error;
          }
        }),
        catchError((err: HttpErrorResponse) => {
          this.httpErrorHandler(err);
          return EMPTY;
        })
      );
  }

  async logout(): Promise<void> {
    await this.api.get('/api/user/logout').subscribe();
    this.deleteSession();
    window.location.reload();
  }

  registerPrivat(name: string, gebDatum: number, mail: string, password: string): Observable<object> {
    return this.api
      .post('/api/user/register/privat', {
        name: name,
        geburtstag: gebDatum,
        mail: mail,
        password: password
      }, {observe: 'response', responseType: 'text'})
      .pipe(
        shareReplay(),
        tap((res: HttpResponse<any>) => {
          if (!this.createSession(res)) {
            throw Error;
          }
        }),
        catchError((err: HttpErrorResponse) => {
          this.httpErrorHandler(err);
          return EMPTY;
        })
      );
  }

  registerUnternehmen(firma: string, name: string, mail: string, password: string): Observable<object> {
    return this.api
      .post('/api/user/register/unternehmen', {
        firmenname: firma,
        name: name,
        mail: mail,
        password: password
      }, {observe: 'response', responseType: 'text'})
      .pipe(
        shareReplay(),
        tap((res: HttpResponse<any>) => {
          if (!this.createSession(res)) {
            throw Error;
          }
        }),
        catchError((err: HttpErrorResponse) => {
          this.httpErrorHandler(err);
          return EMPTY;
        })
      );
  }

  //
  //  SESSION
  //

  // Erstellt eine Session, in dem der Access token gespeichert wird
  // und weitere Daten geladen werden
  createSession(res: HttpResponse<any>): boolean {
    const token = res.headers.get('x-access-token');
    if (token) {
      localStorage.setItem('x-access-token', token);
      window.location.reload();
      return true;
    }
    return false;
  }

  // Löscht die momentane Session
  deleteSession(): void {
    localStorage.clear();
  }

  //
  //  USER
  //

  getProfile(): Observable<object> {
    return this.api.get('/api/user/me');
  }

  getAccessToken(): string {
    return localStorage.getItem('x-access-token');
  }

  //
  //  ERROR HANDLER
  //

  // Error Handler
  // Kann die HTTP Error codes behandeln
  httpErrorHandler(err: HttpErrorResponse): void {
    this.deleteSession();
  }
}

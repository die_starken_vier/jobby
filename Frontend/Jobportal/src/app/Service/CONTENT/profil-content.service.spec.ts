import { TestBed } from '@angular/core/testing';

import { ProfilContentService } from './profil-content.service';

describe('ProfilContentService', () => {
  let service: ProfilContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfilContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

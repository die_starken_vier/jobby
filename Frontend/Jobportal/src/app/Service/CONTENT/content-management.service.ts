import { Injectable } from '@angular/core';
import {Content} from "./content.enum";
import {AuthService} from "../AUTH/auth.service";
import {Observable, of, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContentManagementService {

  content: Content = Content.HOME;
  isProfilePrivate = new Subject<boolean>();
  isLoggedIn: boolean = false;

  name = new Subject<string>();
  benachrichtigung: number;

  anzeigeID: string;


  constructor() { }
}

import { Injectable } from '@angular/core';
import {ProfilSeiten} from "../../Seiten/profil/profil-sidebar/profil-seiten.enum";

@Injectable({
  providedIn: 'root'
})
export class ProfilContentService {

  selected: ProfilSeiten = ProfilSeiten.vorstellungsprofil;

  constructor() { }
}

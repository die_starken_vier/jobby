import { TestBed } from '@angular/core/testing';

import { ProfilLadenService } from './profil-laden.service';

describe('ProfilLadenService', () => {
  let service: ProfilLadenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfilLadenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

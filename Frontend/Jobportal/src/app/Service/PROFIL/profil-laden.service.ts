import {Injectable} from '@angular/core';
import {ApiRequestService} from "../API/api-request.service";
import {ProfilPrivat} from "../../Model/profil-privat";
import {Kontakt} from "../../Model/kontakt.enum";
import {ProfilUnternehmen} from "../../Model/profil-unternehmen";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProfilLadenService {

  constructor(private api: ApiRequestService) {
  }

  //
  //  PRIVAT
  //

  // Lädt sein eigenes Profil
  privatLaden(): Observable<object> {
    return this.api.get('/api/user/me/privat');
  }

  privatSpeichern(profil: ProfilPrivat): void {
    this.api.post('/api/user/me/privat/change', {
      name: profil.name,
      geburtsdatum: profil.geburtsdatum,
      bild: profil.bild,
      adresse: {
        strasse: profil.adresse.strasse,
        nummer: profil.adresse.nummer,
        plz: profil.adresse.plz,
        ort: profil.adresse.ort,
        land: profil.adresse.land
      },
      kontaktangaben: [...profil.kontaktangaben],
      bildung: [...profil.bildung],
      berufserfahrung: [...profil.berufserfahrung],
      kenntnisse: profil.kenntnisse,
      customBox: [...profil.customBox]
    }, {responseType: 'text'}).subscribe();
  }

  //
  //  UNTERNEHMEN
  //

  unternehmenLaden():  Observable<object> {
    return this.api.get('/api/user/me/unternehmen');
  }


  // TODO: IST KAPUTT
  unternehmenSpeichern(profil: ProfilUnternehmen): void {
    this.api.post('/api/user/me/unternehmen/change', { profil }).subscribe();
  }
}

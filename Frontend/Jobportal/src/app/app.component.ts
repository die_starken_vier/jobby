import {Component, OnInit} from '@angular/core';
import {AuthService} from "./Service/AUTH/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Jobbys';

  constructor(private auth: AuthService) {
  }

  ngOnInit() {
    if (this.auth.getAccessToken()) {
      this.auth.getProfile();
    }
  }
}

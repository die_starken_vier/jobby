export interface Bewerbung {
  _id: string,
  titel: string,
  content: string,
  unternehmen: string
}

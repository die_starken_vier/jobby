export interface Session {
  _id: string,
  ip: string,
  token: string,
  last_active: Date
}

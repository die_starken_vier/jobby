export interface Anzeige {
  titel: string,
  content: string,
  _id: string,
  unternehmen: string
}

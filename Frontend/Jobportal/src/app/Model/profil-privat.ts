import {Adresse} from "./adresse";
import {Kontakt} from "./kontakt.enum";

export interface ProfilPrivat {
  name: string;
  geburtsdatum: Date;
  alter: number;
  bild: string;
  adresse: Adresse;
  kontaktangaben: {kontaktmoeglichkeit: Kontakt, angabe: string}[];
  bildung: {schule: string, von: Date, bis: Date, abschluss: string}[];
  berufserfahrung: {firma: string, bezeichnung: string, von: Date, bis: Date}[];
  kenntnisse: string;
  customBox: {titel: string, content: string}[];
}

export interface Adresse {
  strasse: string,
  nummer: string,
  plz: number,
  ort: string,
  land: string
}

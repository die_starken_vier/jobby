import {Adresse} from "./adresse";
import {Kontakt} from "./kontakt.enum";

export interface ProfilUnternehmen {
  name: string;
  bild: string;
  homepage: string;
  adresse: Adresse;
  kontaktangaben: {kontakt: Kontakt, angabe: string}[];
  customBox: {titel: string, content: string}[];
}

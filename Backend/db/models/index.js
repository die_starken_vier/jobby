const {Anzeige} = require("./anzeige.model");
const { User } = require('./user.model');

module.exports = {
  User,
  Anzeige
};

const mongoose = require('mongoose');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const moment = require('moment-timezone');
const uuidTokenGenerator = require('uuid-token-generator');

// JWT Secret
// TODO: Token auslagern und vorm Release ändern
const jwtSecret = "ijuotghrsijo438vcm589gvbFGFFuoih435t89uz4t3ejknlcxv89u453mnk/TFG)ihbujpdfgonjkjnohgfdshnk";

// ** Instance methods **

const UserSchema = new mongoose.Schema({
    firmenname: {
        type: String,
        minLength: 1,
        trim: true,
        unique: true,
        sparse: true
    },
    name: {
        type: String,
        minLength: 1,
        trim: true,
        required: true
    },
    geburtsdatum: {
        type: Date
    },
    mail: {
        type: String,
        minLength: 5,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minLength: 8
    },
    nutzer_typ : {
        type: Number,
        required: true
    },
    rechte: {
        admin: {
            type: Boolean,
            default: false
        }
    },
    banned: {
        type: Boolean,
        default: false
    },
    reg_date: {
        type: Date
    },
    vorstellungsprofil: {
        homepage: [{
            link: {
                type: String,
                trim: true
            }
        }],
        adresse: [{
            strasse: {
                type: String,
                trim: true
            },
            nummer: {
                type: String,
                trim: true
            },
            plz: {
                type: String,
                trim: true
            },
            ort: {
                type: String,
                trim: true
            },
            land: {
                type: String,
                trim: true
            }
        }],
        kontaktangaben: [{
            kontaktmoeglichkeit: {
                type: String,
                trim: true
            },
            angabe: {
                type: String,
                trim: true
            }
        }],
        bild: {
            type: String,
            trim: true,
            default: 'iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAIAAACzY+a1AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAXPSURBVHhe7dgxUhxJEIVhmZgyMfcoHIMjcA4cLkAEJofA0GW4ByZbof5zNnt7mikxM13vtfKzVpk13Vn5gh2kH5/FXEVoryK0VxHaqwjtVYT2KkJ7FaG9itBeRWivIrRXEdqrCO1VhPYqQnsVob2K0F5FaK8itFcR2qsI7VWE9ipCexWhvYrQXkVoryK0VxHaqwjtVYT27CP8sYL2X8DyqqTUjY/tlNn1yORbeMTu2FyMHM7G43bE4Ers/qJ49C6oX4aVr+PcAu11nPMnfROWfQwnOvCBYzhhTvcarHmB9h/iwwu0nYnegQXP0TsDD5qjZ0vxAqx2jt7ZeNwcPU8eEdK4EB6a0PAkNz1LTWhcFI9OaBhSj5DqFfCCQNWQ1uisM6FxBbwgoeFGOkKqV8NrAlU3QnOzyITG1fCahIYV3QipLry/vz8/Pz8+Pr69vVFa0XOSlwWqVvwibKlw4tTGe07SDlSt+EXYfqo4cWrjPSdpJzR8qEzM/gLVY9r/FaczLSFKKzpPTmcOqPrwi/DieGWg6mMnEf769euf315fXyl145WBqo+dRNjCmz54c3NDqdv0wQOqPnYS4dPT0/TBh4cHSt2mDx5Q9bGTCM/BKwNVH34Rtq+9u7u7/u+8k+d5ZaDqwy/C29vbdqB953WmeDjPnxd+v/A/VH0ITcwKA9WFFsZ0oPM3l+lww5/n6CU0fPhFePjNpTn5g/jy8sLRvgipWvGLsMk/iF+k2L4Ff/78OZ28v7+nOjd1D6ha0Y2wobGQfxDXUsz5NR8fHzQSegkNK1pDs8hA9ZicYpP/Xab9x+Fv+pO1vyzSDlTdSEfY0Djmfymu6cyvoeFGbm7WGaiuOJniF/9Yw4lA1ZB6hA2Ndcsg2y8vR7/8DjiX0DCkODpLTWhcCA9NaHjyiLChdzYeN0fPk+j0rHaO3hl40Bw9W7oXYMELtP8QH16g7Uz6Dqz5GE504APHcMKc+jVY9jrOLdBexzl/Bjdh5RfFo3fB5jLs/mw8bkfMrkQO38IjducvinDCg3bE5kokcCE8dBcMLsPWr4AXmFO/Bsv+EkcXaH+Jo85078COV3CoGx9bwSFPotOz2mM48S084hhOGFIcnaUu0D4bj1ug7UZubta5QPtCeOgCbStaQ7PIOXpXwAvm6PkQmpgVztG7Gl4zR8+Eyrgsb47elfGyOXoOJGZlbXP0NsEr5+jJE42QxoZ4cUJD3vhBWVhCY3O8PqGhbfCUrCqhMQhDJDSE1cpmGCKhIWzkiCwpoTEUoyQ0VAlFSFUAAyU0JA0bjt0kNAQwUEJD0pjhWExCQwZjJTT0VISrmCxQ1SMRIVUxDBeo6hkwGStJaIhhuISGmPERUpXEiIGqmK3HYhkJDUmMmNBQMjhCqsIYNFBVUhGewKCBqpJNZ2INCQ1hDJrQkDEyQqryGDdQlVERnsa4gaqMivA0xg1UZWw3EAtIaMhj3ISGhmERUjXB0IGqhoqwC0MHqhoqwi4MHahqqAi7MHSgqqEi7MLQgaqGirAXcweqAjYahXsHqlYYPVAVUBH2YvRAVUBF2IvRA1UBFWEvRg9UBVSEvRg9UBVQEfZi9EBVQEXYi9EDVQEVYS9GD1QFVIS9GD1QFVAR9mL0QFVARdiL0QNVARVhL0YPVAVsNwpXD1R9MHegKqAi7MLQgaqGirALQweqGirCLgwdqGqoCLswdKCqoSLswtCBqoZhETY05DFuQkPDptOwgEBVHuMGqjIqwtMYN1CVURGexriBqoyRETY0hDFoQkPG1gOxhkBVGIMGqkoqwhMYNFBVMjjChoYkRkxoKBkwE8sIVCUxYqAqZnyEDQ0xDJfQEDNmLFYSqIphuEBVj0SEDQ0lTBao6qkIj2OshIaeYZOxmISGAAZKaEiqCI9goISGpJHDsZ6ExlCMktBQNXg+lpTQGIQhEhrCamUzDJHQEDZ+RFaV0Ngcr09oaJOYkoUlNDbEi+foKfv8/BeE+nzz8g04JQAAAABJRU5ErkJggg=='
        },
        bildung: [{
            schule: {
                type: String,
                trim: true
            },
            von: {
                type: Date
            },
            bis: {
                type: Date
            },
            abschluss: {
                type: String,
                trim: true
            }
        }],
        berufserfahrung: [{
            firma: {
                type: String,
                trim: true
            },
            bezeichnung: {
                type: String,
                trim: true
            },
            von: {
                type: Date
            },
            bis: {
                type: Date
            }
        }],
        kenntnisse: {
            type: String,
            trim: true
        },
        customBox: [{
            titel: {
                type: String,
                trim: true
            },
            content: {
                type: String,
                trim: true
            }
        }]
    },
    sessions: [{
        ip: {
          type: String,
          required: true
        },
        token: {
            type: String,
            required: true
        },
        last_active: {
            type: Date
        }
    }]
});

//
//  Überschreibt den Return Wert
//  Sorgt dafür, dass das Passwort und die Sessions Standardmäßig nicht wiedergegeben werdenü
//
UserSchema.methods.toJSON = function () {
    const user = this;
    const userObject = user.toObject();

    return _.omit(userObject, ['password', 'sessions']);
}

//
//  Erstellt einen Zugangstoken
//
UserSchema.methods.generateAccessAuthToken = function () {
    const user = this;
    return new Promise(((resolve, reject) => {
        // Erstellt einen JSON Web Token und gibt ihn wieder
        const uuid = createUUIDToken();

        jwt.sign({ _id: user._id.toHexString(), sessionToken: uuid }, jwtSecret, (err, token) => {
            if (!err) {
                resolve({token, uuid});
            } else {
                reject();
            }
        })
    }));
}

//
//  Erstellt eine Session für den User
//
UserSchema.methods.createUserSession = function (ip) {
    let user = this;
    return user.generateAccessAuthToken().then((token) => {
        return saveSessionToDatabase(user, ip, token);
    }).then((token) => {
        return token.token;
    }).catch((e) => {
        return Promise.reject('Failed to save session to database.\n' + e);
    });
}

/* MODEL METHODS (static) */

//
//  Gibt den JWT Secret wieder, für die Erstellung des JWT
//
UserSchema.statics.getJWTSecret = () => {
    return jwtSecret;
}

//
//  Sucht einen User mit der ID und dem Token
//  Wenn Korrekte Kombination: gibt den User wieder
//  Wenn Falsche Kombination: gibt nichts wieder
//
UserSchema.statics.findByIdAndToken = function (_id, token) {
    const User = this;

    User.findOne({
        _id: _id,
        'sessions.token': token
    }).then(user => {
        if (user) {
            // Iteriert über die User Sessions mit O(n/2) und Updated last_active, speichert dies in der DB
            // und gibt anschließend den User wieder
            for (let session of user.sessions) {
                if (session.token === token) {
                    session.last_active = moment.utc().local(false).toDate();
                    break;
                }
            }
            user.save();
        }
    });

    // Gibt den Gefundenen User wieder, nachdem last_active geupdated wurde
    return User.findOne({
        _id: _id,
        'sessions.token': token
    });
}

//
//  Sucht einen User mittels login daten
//
UserSchema.statics.findByCredentials = function (_mail, password) {
    let User = this;
    return User.findOne({
        mail: _mail.toLowerCase()
    }).then((user) => {
        // Wenn es keinen User mit der Mail gibt, gebe einen Fehler zurück
        if (!user)
            return Promise.reject();

        // Wenn es den User gibt wird Asynchron überprüft ob das Password übereinstimmt
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err,res) => {
                // Password Korrekt: Gibt den User wieder
                // Password Falsch: Gibt einen Fehler zurück
                if (res) resolve(user);
                else reject();
            })
        })
    });
}

//
//  Gibt alle User aus
//
UserSchema.statics.getAllUser = function () {
    let User = this;
    return User.find();
}

/* MIDDLEWARE */

//
//  Überschreibt die Speicher Funktion
//  Hashed das Passwort
//
UserSchema.pre('save', function (next) {
    let user = this;

    // Wenn das Password Feld modifiziert wurde, Hashe es
    if (user.isModified('password')) {
        const costFactor = 10;

        user.password = bcrypt.hashSync(user.password, costFactor);
    }

    // Wenn das Mail Feld modifiziert wurde, mache es Lowercase
    if (user.isModified('mail')) {
        user.mail = user.mail.toLowerCase();
    }

    next();
})

/* HELPER METHODS */

//
//  Speicher die momentane Session in der Datenbank
//
let saveSessionToDatabase = (user, _ip, token) => {
    return new Promise((resolve, reject) => {
        if (user && _ip && token.uuid) {
            const zeit = moment.utc().local(false).toDate();
            user.sessions.push({'ip': _ip, 'token': token.uuid, 'last_active': zeit});

            user.save().then(() => {
                return resolve(token);
            }).catch((e) => {
                reject(e);
            });
        } else {
            reject();
        }
    })
}

//
//  Erstellt einen UUID Token
//
let createUUIDToken = () => {
    const tokenGenerator = new uuidTokenGenerator();
    return tokenGenerator.generate();
}

// Speicher das Modell als Konstante
const User = mongoose.model('User', UserSchema);

// Exportiert das Modell
module.exports = { User }

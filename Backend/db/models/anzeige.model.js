const mongoose = require('mongoose');
const {Status} = require('../../anzeige/status.enum');

const AnzeigeSchema = new mongoose.Schema({
    titel: {
        type: String,
        trim: true,
        required: true
    },
    content: {
        type: String,
        trim: true,
        required: true
    },
    unternehmen: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    lat: {
        type: String,
        trim: true,
        required: true
    },
    long: {
        type: String,
        trim: true,
        required: true
    },
    bewerbungen: [{
        user: {
            type: mongoose.Types.ObjectId,
            required: true
        },
        status: {
            type: Status,
            default: Status.NEU,
            required: true
        },
        anschreiben: {
            type: String,
            required: true
        }
    }]
});

AnzeigeSchema.statics.findMyAnzeigen = function (user_id) {
    const Anzeige = this;

    return Anzeige.find({
      unternehmen: user_id
    });
}

AnzeigeSchema.statics.findMyBewerbungen = function (user_id) {
    const Anzeige = this;

    return Anzeige.find({
        'bewerbungen.user': user_id
    });
}

// Speicher das Modell als Konstante
const Anzeige = mongoose.model('Anzeige', AnzeigeSchema);

// Exportiert das Modell
module.exports = { Anzeige }

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

let db = 'mongodb://localhost:27017/Jobby';

if (process.env.PROD) {
    db = 'mongodb://db:27017/Jobby'
}

mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true}).then(() => {
    console.log("Erfolgreich Verbunden!")
}).catch((e) => {
    console.log("Bei der Verbindung zu MongoDB ist ein fehler aufgetreten");
    console.log(e);
});

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = {
    mongoose
};

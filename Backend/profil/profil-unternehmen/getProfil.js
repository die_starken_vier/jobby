const { User } = require('../../db/models');
const _ = require('lodash');

let getProfilUnternehmen = (req,res) => {

    User.findOne({
        _id: req.user_id

    }).then(myself => {
        const exception = _.omit(myself.toObject(), ['_id', '__v', 'password', 'sessions', 'vorstellungsprofil.bildung', 'vorstellungsprofil.berufserfahrung'
            ,'vorstellungsprofil.berufserfahrung','vorstellungsprofil.kenntnisse']);
        res.send(exception);
    })
        .catch(() => {
            res.sendStatus(400);
        })
}

module.exports = {
    getProfilUnternehmen
}
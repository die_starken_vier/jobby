const { User } = require('../../db/models');
const _ = require('lodash');

let setProfilUnternehmen = (req,res) => {

    let name,homepage, bild, adresse, kontaktangaben, customBox;

    // Liest die Daten aus der Request aus
    try {
        name = req.body.name;
        homepage = req.body.homepage;
        bild = req.body.bild;
        adresse= req.body.adresse;
        kontaktangaben = req.body.kontaktangaben;
        customBox = req.body.customBox;

        // Wenn es fehlerhaft ist, wird es abgefangen
    } catch (_) {
        res.sendStatus(400);
        return;
    }
    // Sucht sich selbst
    User.findOne({
        _id: req.user_id

        //Schreibt die Daten aus Request in den Profil ein
    }).then(profil =>{

        profil.name = name;

        //*Weil homepage ein Array ist, wird es am anfang geleert und dann schreibt alle Angaben
        profil.vorstellungsprofil.homepage = [];
        for(let homp of homepage){
            profil.vorstellungsprofil.homepage.push({
                link: homp.link
            });
        }

        profil.vorstellungsprofil.bild = bild;

        profil.vorstellungsprofil.adresse = {
            strasse: adresse.strasse,
            nummer: adresse.nummer,
            plz: adresse.plz,
            ort: adresse.ort,
            land: adresse.land
        };

        profil.vorstellungsprofil.kontaktangaben = [];
        for(let kontaktang of kontaktangaben){
            profil.vorstellungsprofil.kontaktangaben.push({
                kontaktmoeglichkeit: kontaktang.kontaktmoeglichkeit,
                angabe: kontaktang.angabe

            });
        }

        profil.vorstellungsprofil.customBox = [];
        for(let custom of customBox){
            profil.vorstellungsprofil.customBox.push({
                titel: custom.titel,
                content: custom.content
            });
        }

        profil.save();
        res.sendStatus(200);
    })
        .catch(() => {
            res.sendStatus(400);
        })

}


module.exports = {
    setProfilUnternehmen
}
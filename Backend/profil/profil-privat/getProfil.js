const { User } = require('../../db/models');
const _ = require('lodash');

let getProfilPrivat = (req,res) => {

    User.findOne({
        _id: req.user_id

    }).then(myself => {
        const exception = _.omit(myself.toObject(), ['_id', '__v', 'password', 'sessions', 'vorstellungsprofil.homepage']);
        res.send(exception);
    })
        .catch(() => {
            res.sendStatus(400);
        })
}

module.exports = {
    getProfilPrivat
}
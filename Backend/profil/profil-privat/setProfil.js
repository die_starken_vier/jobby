const {User} = require('../../db/models');
const _ = require('lodash');

let setProfilPrivat = (req, res) => {

    let name, geburtsdatum, bild, adresse, kontaktangaben, bildung, berufserfahrung, kenntnisse, customBox;

    // Liest die Daten aus der Request aus
    try {
        name = req.body.name;
        geburtsdatum = Date(req.body.geburtsdatum);
        bild = req.body.bild;
        adresse = req.body.adresse;
        kontaktangaben = req.body.kontaktangaben;
        bildung = req.body.bildung;
        berufserfahrung = req.body.berufserfahrung;
        kenntnisse = req.body.kenntnisse;
        customBox = req.body.customBox;

        // Wenn es fehlerhaft ist, wird es abgefangen
    } catch (_) {
        res.sendStatus(400);
        return;
    }
    // Sucht sich selbst
    User.findOne({
        _id: req.user_id

        //Schreibt die Daten aus Request in den Profil ein
    }).then(profil => {

        profil.name = name;
        profil.geburtsdatum = geburtsdatum;
        profil.vorstellungsprofil.bild = bild;
        profil.vorstellungsprofil.adresse = {
            strasse: adresse.strasse,
            nummer: adresse.nummer,
            plz: adresse.plz,
            ort: adresse.ort,
            land: adresse.land
        };
        //*Weil kontaktangaben ein Array ist, wird es am anfang geleert und dann schreibt alle Angaben
        profil.vorstellungsprofil.kontaktangaben = [];
        if (kontaktangaben) {
            for (let kontaktang of kontaktangaben) {
                profil.vorstellungsprofil.kontaktangaben.push({
                    kontaktmoeglichkeit: kontaktang.kontaktmoeglichkeit,
                    angabe: kontaktang.angabe

                });
            }
        }

        profil.vorstellungsprofil.bildung = [];
        if (bildung) {
            for (let bil of bildung) {
                profil.vorstellungsprofil.bildung.push({
                    schule: bil.schule,
                    von: bil.von,
                    bis: bil.bis,
                    abschluss: bil.abschluss
                });
            }
        }

        profil.vorstellungsprofil.berufserfahrung = [];
        if (berufserfahrung) {
            for (let berufserf of berufserfahrung) {
                profil.vorstellungsprofil.berufserfahrung.push({
                    firma: berufserf.firma,
                    bezeichnung: berufserf.bezeichnung,
                    von: berufserf.von,
                    bis: berufserf.bis
                });
            }
        }

        profil.vorstellungsprofil.kenntnisse = kenntnisse;

        profil.vorstellungsprofil.customBox = [];
        if (customBox) {
            for (let custom of customBox) {
                profil.vorstellungsprofil.customBox.push({
                    titel: custom.titel,
                    content: custom.content
                });
            }
        }

        profil.save();
        res.sendStatus(200);
    })
        .catch((e) => {
            res.sendStatus(400);
        })

}


module.exports = {
    setProfilPrivat
}

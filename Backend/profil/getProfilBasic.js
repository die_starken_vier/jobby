const {User} = require('../db/models/user.model');
const _ = require('lodash');

let getBasicUser = (req, res) => {
    const user_id = req.user_id;

    User.findOne({
        _id: user_id
    }).then(u => {
        if (u) {
            res.status(200).send(_.pick(u.toObject(), ['name', 'nutzer_typ']));
        } else {
            res.sendStatus(404);
        }
    }).catch(() => {
        res.sendStatus(400);
    })
}

module.exports = {
    getBasicUser
}

const {logout} = require("./logout");
const {login} = require("./login");
const {rechteBearbeiten} = require("./rechte-bearbeiten");
const {registerUnternehmen} = require("./register-unternehmen");
const { registerUser } = require('./register-privat');

module.exports = {
    registerUser,
    registerUnternehmen,
    rechteBearbeiten,
    login,
    logout
}

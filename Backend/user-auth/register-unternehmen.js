const { User } = require('../db/models');

let registerUnternehmen = (req, res) => {
    // REQUIRED: firmenname, name, mail, password, nutzer_typ = 1

    let firmenname, name, mail, password;

    const nutzer_typ = 1;
    const admin = false;

    // Liest die Daten aus der Request aus
    try {
        firmenname = req.body.firmenname;
        name = req.body.name;
        mail = req.body.mail;
        password = req.body.password;
    } catch (_) {
        res.sendStatus(400);
        return;
    }

    // Erstellt ein neues User Objekt mit den User Daten
    let neuerUser = new User({
        'firmenname': firmenname,
        'name': name,
        'mail': mail,
        'password': password,
        'nutzer_typ': nutzer_typ,
        'rechte': {
            'admin': admin
        }
    });

    // Speichert den User in der DB und schickt anschließend einen
    // Access JWT damit der User direkt eingeloggt ist
    neuerUser.save()
        .then(() => {
            return neuerUser.createUserSession(req.clientIp);
        })
        .then(authToken => {
            res.header('x-access-token', authToken)
                .sendStatus(200);
        })
        .catch(() => {
            res.sendStatus(400);
        })
};

module.exports = {
    registerUnternehmen
}

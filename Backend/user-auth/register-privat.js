const { User } = require('../db/models');

let registerUser = (req, res) => {
    // REQUIRED: name, geb. datum, mail, password, nutzer_typ = 0
    // OPTIONAL: admin rechte

    let name, gebDatum, mail, password, admin;
    const  nutzer_typ = 0;

    // Liest die Daten aus der Request aus
    try {
        name = req.body.name;
        gebDatum = Date(req.body.geburtsdatum);
        mail = req.body.mail;
        password = req.body.password;
        admin = false;
    } catch (_) {
        res.sendStatus(400);
        return;
    }

    // Erstellt ein neues User Objekt mit den User Daten
    let neuerUser = new User({
        'name': name,
        'geburtsdatum': gebDatum,
        'mail': mail,
        'password': password,
        'nutzer_typ': nutzer_typ,
        'rechte': {
            'admin': admin
        }
    });

    // Speichert den User in der DB und schickt anschließend einen
    // Access JWT damit der User direkt eingeloggt ist
    neuerUser.save()
        .then(() => {
            return neuerUser.createUserSession(req.clientIp);
        })
        .then(authToken => {
            res.header('x-access-token', authToken)
                .sendStatus(200);
        })
        .catch(() => {
            res.sendStatus(400);
        })
};

module.exports = {
    registerUser
}

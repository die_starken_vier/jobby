const { User } = require('../db/models')

let rechteBearbeiten = (req, res) => {
    // Vorraussetzung: Nutzer ist Angemeldet, Nutzer hat Admin Rechte

    // Ausführender Nutzer (adminNutzer): req.user_id
    // Betreffender Nutzer (nutzer)     : req.params.id
    const adminNutzer = req.user_id;
    const nutzer = req.params.id;

    // Neue Rechte
    const neueRechte = req.body.rechte;

    // Überprüft ob beide Werte vorhanden sind
    if (adminNutzer && nutzer) {
        // Sucht den Ausführende Nutzer
        User.findOne({
            _id: adminNutzer
        }).then(admin => {
            // Wenn dieser gefunden wurde, wird überprüft ob dieser Admin Rechte hat
            if (admin.rechte.admin) {
                // Wenn Ja -> Dann geht es weiter
                return nutzer;
            } else {
                // Wenn Nein -> Fehler -> 401 Not Authorized wird gesendet
                throw 'Not Authorized';
            }
        }).then(() => {
            // Nun wird der Referenzierte Nutzer gesucht
            User.findOne({
                _id: nutzer
            }).then(doc => {
                // Und seine Rechte werden zu den neuen Rechten gesetzt
                // + Gespeichert
                doc.rechte = neueRechte;
                doc.save();
                res.sendStatus(200);
            })
        }).catch(() => {
            // Wenn ein Fehler auftritt, oder die Rechte fehlen wird ein 403 gesendet
            res.sendStatus(403);
        })
    } else {
        res.sendStatus(400);
    }

}

module.exports = {
    rechteBearbeiten
}

const { User } = require('../db/models')

let logout = (req, res) => {
    // req.user_id => User ID;
    // req.sessionToken => SessionToken;

    User.findOneAndUpdate({
        _id: req.user_id
    }, {
        $pull: {'sessions': {'token': req.sessionToken}}
    }).then(() => {
        res.sendStatus(200);
    }).catch(() => {
        res.sendStatus(400);
    })
}

module.exports = {
    logout
}

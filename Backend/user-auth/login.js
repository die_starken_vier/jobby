const { User } = require('../db/models');

// Loggt den Benutzer anhand einer E-Mail und einem Passwort ein
let login = (req, res) => {
    const mail = req.body.mail;
    const password = req.body.password;

    if (mail && password) {
        User.findByCredentials(mail, password)
            .then(user => {
                return user.createUserSession(req.clientIp);
            })
            .then(token => {
                res.header('x-access-token', token)
                    .sendStatus(200);
            })
            .catch((_) => {
                res.sendStatus(400);
            })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    login
}

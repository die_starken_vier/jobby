const {Anzeige} = require('../db/models/anzeige.model');

let getBewerbungStatus = (req, res) => {
    const user_id = req.user_id;

    let abort = false;
    let anzeigeID;

    try {
        anzeigeID = req.body.anzeigeID;

        if (!anzeigeID)
            abort = true;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOne({
            _id: anzeigeID,
            'bewerbungen.user': user_id
        })
            .then(a => {
                if (a) {
                    for (let b of a.bewerbungen) {
                        if (b.user.toString() === user_id) {
                            res.status(200).send({
                                status: b.status
                            });
                            break;
                        }
                    }
                } else {
                    res.sendStatus(404);
                }
            })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    getBewerbungStatus
}

const {Anzeige} = require('../db/models/anzeige.model');

let anzeigeBewerben = (req, res) => {
    const user_id = req.user_id;

    let abort = false;
    let anschreiben, anzeigeID;

    try {
        anschreiben = req.body.anschreiben;
        anzeigeID = req.body.anzeigeID;

        if (!anschreiben || !anzeigeID)
            abort = true;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOne({
            _id: anzeigeID
        }).then(a => {
            if (a) {
                if (a.bewerbungen.filter(b => b.user.toString() === user_id).length === 0) {
                    a.bewerbungen.push({
                        user: user_id,
                        anschreiben: anschreiben
                    });
                    a.save();
                    res.sendStatus(200);
                } else {
                    res.sendStatus(403);
                }
            } else {
                res.sendStatus(404);
            }
        }).catch(() => {
            res.sendStatus(400);
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    anzeigeBewerben
}

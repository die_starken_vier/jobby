const {Anzeige} = require('../db/models/anzeige.model');

let anzeigeBearbeiten = (req, res) => {
    const user_id = req.user_id;

    let anzeigeID, titel, content;
    let abort = false;

    try {
        anzeigeID = req.body.anzeigeID;
        titel = req.body.titel;
        content = req.body.content;

        if (!anzeigeID || !titel || !content)
            abort = true;
    } catch (_) {
        abort = true
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOne({
            _id: anzeigeID,
            unternehmen: user_id
        }).then(a => {
            if (a) {
                a.titel = titel;
                a.content = content;
                a.save();
                res.sendStatus(200);
            } else {
                res.sendStatus(404);
            }
        }).catch((e) => {
            res.sendStatus(403);
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    anzeigeBearbeiten
}

const {Anzeige} = require('../db/models/anzeige.model');

let setStatus = (req, res) => {
    const user_id = req.user_id;

    let abort = false;
    let anzeigeID, bewerberID, status;
    try {
        anzeigeID = req.body.anzeigeID;
        bewerberID = req.body.bewerberID;
        status = Number(req.body.status);

        if (!anzeigeID || !bewerberID || !status)
            abort = true;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOne({
            _id: anzeigeID,
            unternehmen: user_id
        }).then(a => {
            if (a) {
                for (let b of a.bewerbungen) {
                    if (b.user.toString() === bewerberID) {
                        b.status = status;
                        break;
                    }
                }
                a.save();
                res.sendStatus(200);
            } else {
                res.sendStatus(404);
            }
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    setStatus
}

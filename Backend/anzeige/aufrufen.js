const {Anzeige} = require('../db/models/anzeige.model');
const _ = require('lodash');

let anzeigeAufrufen = (req, res) => {
    // URL Parameter mit ID
    const anzeigeID = req.params.id;

    if (anzeigeID) {
        Anzeige.findOne({
            _id: anzeigeID
        }).then(a => {
            if (a) {
                const anzeige = _.omit(a.toObject(), ['__v', 'bewerbungen']);
                res.status(200).send(anzeige);
            } else {
                res.sendStatus(400);
            }
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    anzeigeAufrufen
}

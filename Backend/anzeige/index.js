// TODO: Anzeige getBewerbungen
const {anzeigeKarte} = require("./karte");
const {getEigeneBewerbung} = require("./getEigeneBewerbungen");
const {getBewerber} = require("./getBewerbungen");
const {setStatus} = require("./setStatus");
const {anzeigeSuchen} = require("./suchen");
const {bewerbungLoeschen} = require("./bewerbungLoeschen");
const {getBewerbungStatus} = require("./getStatus");
const {anzeigeBewerben} = require("./bewerben");
const {anzeigeBearbeiten} = require("./bearbeiten");
const {anzeigeLoeschen} = require("./loeschen");
const {anzeigeAufrufen} = require("./aufrufen");
const {anzeigeErstellen} = require("./erstellen");

module.exports = {
    anzeigeErstellen,
    anzeigeAufrufen,
    anzeigeLoeschen,
    anzeigeBearbeiten,
    anzeigeBewerben,
    getBewerbungStatus,
    bewerbungLoeschen,
    anzeigeSuchen,
    setStatus,
    getBewerber,
    getEigeneBewerbung,
    anzeigeKarte
}

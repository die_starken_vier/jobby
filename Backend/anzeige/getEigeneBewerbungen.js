const {Anzeige} = require('../db/models/anzeige.model');
const _ = require('lodash');

let getEigeneBewerbung = (req, res) => {
    const user_id = req.user_id;

    Anzeige.find({
        'bewerbungen.user': user_id
    }).then(a => {
        let anzeigen = [];
        for (let anz of a) {
            anzeigen.push(_.omit(anz.toObject(), ['bewerbungen', '__v']))
        }
        res.status(200).send(anzeigen);
    }).catch(() => {
        res.sendStatus(400);
    })
}

module.exports = {
    getEigeneBewerbung
}

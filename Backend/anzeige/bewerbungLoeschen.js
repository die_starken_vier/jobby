const {Anzeige} = require('../db/models/anzeige.model');

let bewerbungLoeschen = (req, res) => {
    const user_id = req.user_id;

    let anzeigeID;
    let abort = false;

    try {
        anzeigeID = req.body.anzeigeID;

        if (!anzeigeID)
            abort = true;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOneAndUpdate({
            _id: anzeigeID,
            'bewerbungen.user': user_id
        }, {
            $pull: {'bewerbungen': {'user': user_id}}
        }).then(() => {
            res.sendStatus(200);
        }).catch(() => {
            res.sendStatus(400);
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    bewerbungLoeschen
}

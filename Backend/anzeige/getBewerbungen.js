const {Anzeige} = require('../db/models/anzeige.model');
const _ = require('lodash');

let getBewerber = (req, res) => {
    const user_id = req.user_id;

    let abort = false;
    let anzeigeID;
    try {
        anzeigeID = req.body.anzeigeID;

        if (!anzeigeID)
            abort = true;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOne({
            _id: anzeigeID,
            unternehmen: user_id
        }).then(a => {
            if (a) {
                let bewerber = []
                for (let b of a.bewerbungen) {
                    bewerber.push(_.omit(b.toObject(), ['_id']))
                }
                res.status(200).send(bewerber);
            } else {
                res.sendStatus(404);
            }
        }).catch(() => {
            res.sendStatus(400);
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    getBewerber
}

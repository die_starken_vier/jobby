const {Anzeige} = require('../db/models/anzeige.model');
const {User} = require('../db/models/user.model');

let anzeigeLoeschen = (req, res) => {
    const user_id = req.user_id;

    let abort = false;

    let anzeigeID;
    try {
        anzeigeID = req.body.anzeigeID;
        if (!anzeigeID)
            abort = true;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.findOneAndRemove({
            _id: anzeigeID,
            unternehmen: user_id
        })
            .then(() => {
                res.sendStatus(200);
            })
            .catch(() => {
                res.sendStatus(400);
            })
    } else {
        res.sendStatus(400)
    }
}

module.exports = {
    anzeigeLoeschen
}

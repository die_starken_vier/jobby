const {Anzeige} = require('../db/models/anzeige.model');
const {User} = require('../db/models/user.model');

let anzeigeErstellen = (req, res) => {
    const user_id = req.user_id;
    let abort = false;

    let titel, content;
    try {
        titel = req.body.titel;
        content = req.body.content;

        if (!titel || !content)
            abort = true;
    } catch (_) {
        abort = true;
    }

    if (!abort) {
        const neueAnzeige = new Anzeige({
            titel: titel,
            content: content,
            unternehmen: user_id
        });

        User.findOne({
            _id: user_id
        }).then(u => {
            // Überprüft ob es ein Unternehmen ist
            if (u.nutzer_typ === 1) {
                neueAnzeige.save()
                    .then(() => {
                        res.sendStatus(200);
                    })
                    .catch(() => {
                        res.sendStatus(400);
                    });
            } else {
                res.sendStatus(403);
            }
        }).catch(() => {
            res.sendStatus(403);
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    anzeigeErstellen
}

const Status = {
    NEU: 0,
    IN_BEARBEITUNG: 1,
    ANGENOMMEN: 2,
    ABGELEHNT: 3
}

module.exports = {
    Status
}

const {Anzeige} = require('../db/models/anzeige.model');
const _ = require('lodash');
const {User} = require('../db/models/user.model');


let anzeigeKarte = async (req, res) => {
    Anzeige.find()
        .then((a) => {
            //     return _.omit(userObject, ['password', 'sessions']);
            let anzKarte = ""

            User.find({
                nutzer_typ: 1
            }).then(u => {
                for (const anz of a) {
                    const firma = u.filter(uf => {
                        return uf._id.toString() === anz.unternehmen.toString()
                    })[0].firmenname;

                    if (firma && anz.long && anz.lat) {
                        anzKarte += `${firma},${anz.titel},${anz.lat},${anz.long},${anz._id}\n`;
                    }
                }

                res.send(anzKarte);
            }).catch((e) => {
                console.log(e);
                res.sendStatus(400);
            });
        })
        .catch((e) => {
            console.log(e);
            res.sendStatus(400);
        })

}

module.exports = {
    anzeigeKarte
}

const {Anzeige} = require('../db/models/anzeige.model');
const _ = require('lodash');

let anzeigeSuchen = (req, res) => {
    let abort = false;
    let suchTerm;
    try {
        suchTerm = req.body.suche;
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        Anzeige.find({
            titel: {$regex: suchTerm, $options: 'i'}
        }).then(a => {
            let anzeigen = [];
            a.forEach(anz => {
                anzeigen.push(_.omit(anz.toObject(), ['__v', 'bewerbungen']));
            })
            res.status(200).send(anzeigen);
        }).catch((e) => {
            res.sendStatus(400);
        })
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    anzeigeSuchen
}

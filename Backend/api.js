const express = require('express');
const app = express();

// Lade Zeitzonen Bibliothek
const moment = require('moment-timezone');

// Lade Datenbank Verbindung
const { db } = require('./db');
const { User, Anzeige } = require('./db/models')

// Lade JWT Bibliothek
const jwt = require('jsonwebtoken');

// Lade RequestIP Bibliothek
const requestiP = require('request-ip');
const {anzeigeKarte} = require("./anzeige");

// Lade Registrierung
const { registerUser, registerUnternehmen, rechteBearbeiten, login, logout} = require('./user-auth');

//Lade Profildaten für Vorstellungsprofil für Privat Person
const {getProfilPrivat} = require('./profil/profil-privat/getProfil');

//Lade Bearbeitung der Profildaten für Vorstellungsprofil für Privat Person
const {setProfilPrivat} = require('./profil/profil-privat/setProfil');

//Lade Profildaten für Vorstellungsprofil für Unternehmen
const {getProfilUnternehmen} = require('./profil/profil-unternehmen/getProfil');

//Lade Bearbeitung der Profildaten für Vorstellungsprofil für Unternehmen
const {setProfilUnternehmen} = require('./profil/profil-unternehmen/setProfil');

// Lade GetBasicProfil
const {getBasicUser} = require('./profil/getProfilBasic');

// Lade Account Management
const { changeMyMail, changeMyPass, getSessions, logoutSession } = require('./account-management');

// Lade Anzeige
const { anzeigeErstellen, anzeigeAufrufen, anzeigeLoeschen, anzeigeBearbeiten,
        anzeigeBewerben, getBewerbungStatus, bewerbungLoeschen, anzeigeSuchen,
        setStatus, getBewerber, getEigeneBewerbung } = require('./anzeige');

// Server Port
const port = 3000;

/* MIDDLEWARE */

app.use(express.json());

// CORS Header
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token, _id");

    res.header(
        'Access-Control-Expose-Headers',
        'x-access-token'
    );

    next();
});

// Request IP Middleware
app.use(requestiP.mw());

// Authentifiziert den JWT
let authenticate = (req, res, next) => {
     let token = req.get('x-access-token');

     // Verifizieren
    jwt.verify(token, User.getJWTSecret(), (err, decoded) => {
       if (err) {
           // !!! INVALIDE ODER NICHT VORHANDEN !!!
           // !!! NICHT AUTHENTIFIZIEREN        !!!
           res.sendStatus(401);
       } else {
           // Token: _id => User ID, sessionToken => Session Token
           req.user_id = decoded._id;
           req.sessionToken = decoded.sessionToken;

           // Token ist Vorhanden und Valide
           // Nun wird überprüft ob die Session noch Vorhanden ist

           User.findByIdAndToken(req.user_id, req.sessionToken)
               .then(user => {

                   // Session wurde gefunden -> Session kann fortgeführt werden
                   if (user) {
                       next();
                   } else {
                       throw 'Session nicht gefunden';
                   }
               })
               .catch((err) => {
                   // Session wurde nicht gefunden -> Session nicht Vorhanden -> Abbruch
                   res.sendStatus(401);
               })
       }
    });
};

/* ROUTE HANDLER */

//
//  POST /user/register/privat
//  Aufgabe: Erhält Daten von Privaten Nutzern für die Registrierung
//
app.post('/api/user/register/privat', registerUser);

//
//  POST /user/register/unternehmen
//  Aufgabe: Erhält Daten von Unternehmen Nutzern für die Registrierung
//
app.post('/api/user/register/unternehmen', registerUnternehmen);

//
//  POST /user/:id/rechte
//  Variable: id -> User ID
//  Aufgabe: Bearbeitet die Rechte des Users
//
app.post('/api/user/:id/rechte', authenticate, rechteBearbeiten);

//
//  POST /user/login
//  Aufgabe: Loggt den Nutzer ein
//
app.post('/api/user/login', login);

//
//  GET /user/logout
//  Aufgabe: Loggt die momentane Session aus
//
app.get('/api/user/logout', authenticate, logout);


//
//  GET /user/me
//  Aufgabe: Gibt Name + Nutzer_typ wieder
//
app.get('/api/user/me', authenticate, getBasicUser);

//
//  GET /user/me/privat
//  Aufgabe: Gibt die Profildaten für Vorstellungsprofil für Privat Person aus
//
app.get('/api/user/me/privat', authenticate, getProfilPrivat)


//
//  GET /user/me/privat
//  Aufgabe: Ändert die Profildaten für Vorstellungsprofil für Privat Person
//
app.post('/api/user/me/privat/change', authenticate, setProfilPrivat)


//
//  GET /user/me/privat
//  Aufgabe: Gibt die Profildaten für Vorstellungsprofil für Unternehmen aus
//
app.get('/api/user/me/unternehmen', authenticate, getProfilUnternehmen)

//
//  GET /user/me/privat
//  Aufgabe: Ändert die Profildaten für Vorstellungsprofil für Unternehmen
//
app.post('/api/user/me/unternehmen/change', authenticate, setProfilUnternehmen)

//
//  POST /user/me/mail/change
//  Aufgabe: Ändert seine eigene E-Mail
//
app.post('/api/user/me/mail/change', authenticate, changeMyMail);

//
//  POST /user/me/pass/change
//  Aufgabe: Ändert sein eigenes Passwort
//
app.post('/api/user/me/pass/change', authenticate, changeMyPass);

//
//  GET /user/me/sessions
//  Aufgabe: Erhält seine Sessions
//
app.get('/api/user/me/sessions', authenticate, getSessions);

//
//  POST /user/me/sessions/logout
//  Aufgabe: Loggt seine Session anhand der Session ID aus
//
app.post('/api/user/me/sessions/logout', authenticate, logoutSession);

//
//  POST /anzeige/erstellen
//  Aufgabe: Speichert eine neue Anzeige
//
app.post('/api/anzeige/erstellen', authenticate, anzeigeErstellen);

//
//  GET /anzeige/karte
//  Aufabe: Gibt alle Anzeigen mit ihren Koordinaten wieder
//
app.get('/api/anzeige/karte', anzeigeKarte);

//
//  GET /anzeige/:id
//  Variable: id -> AnzeigenID
//  Aufgabe: Gibt eine Anzeige wieder
//
app.get('/api/anzeige/:id', anzeigeAufrufen);

//
//  POST /anzeige/loeschen
//  Aufgabe: Löscht eine Anzeige
//
app.post('/api/anzeige/loeschen', authenticate, anzeigeLoeschen);

//
//  POST /anzeige/bearbeiten
//  Aufgabe: Bearbeitet eine seiner Anzeigen
//
app.post('/api/anzeige/bearbeiten', authenticate, anzeigeBearbeiten);

//
//  POST /anzeige/bewerbung/erstellen
//  Aufgabe: Bewerbungen der Anzeige hinzufügen
//
app.post('/api/anzeige/bewerbung/erstellen', authenticate, anzeigeBewerben);

//
//  POST /anzeige/bewerbung/status
//  Aufgabe: Gibt den Status der Bewerbung wieder
//
app.post('/api/anzeige/bewerbung/status', authenticate, getBewerbungStatus);

//
//  POST /anzeige/bewerbung/loeschen
//  Aufgabe: Löscht seine eigene Bewerbung
//
app.post('/api/anzeige/bewerbung/loeschen', authenticate, bewerbungLoeschen);

//
//  POST /anzeige/suchen
//  Aufgabe: Gibt alle Anzeigen wieder, die dem Suchkriterium übereinstimmen
//
app.post('/api/anzeige/suchen', anzeigeSuchen);

//
//  POST /anzeige/bewerbung/status/setzen
//  Aufgabe: Lässt den Anzeigenersteller den Status eines Bewerbers setzen
//
app.post('/api/anzeige/bewerbungen/status/setzen', authenticate, setStatus);

//
//  POST /anzeige/bewerbung/bewerber
//  Aufgabe: Erhält alle Bewerber seiner Anzeige
//
app.post('/api/anzeige/bewerbung/bewerber', authenticate, getBewerber);

//
//  GET /anzeige/bewerbung/me
//  Aufgabe: Erhält eigene Bewerbungen
//
app.get('/api/anzeige/bewerbung/me', authenticate, getEigeneBewerbung);

app.use(express.static('public'));

app.get('/assets/scripts/karte.js', (req, res) => {
    res.sendFile(__dirname + '/public/assets/scripts/karte.js');
})

app.all('/*', (req, res) => {
    const url = req.params[0];
    res.sendFile('index.html', { root: __dirname + '/public'})

});

app.listen(port, () => {
   console.log(`Der Server ist auf dem Port ${port} Erreichbar!`);
});

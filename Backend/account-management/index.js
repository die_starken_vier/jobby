const {logoutSession} = require("./change-self");
const {getSessions} = require("./change-self");
const {changeMyPass, changeMyMail} = require('./change-self')

module.exports = {
    changeMyMail,
    changeMyPass,
    getSessions,
    logoutSession
}

const {User} = require('../db/models');
const bcrypt = require('bcryptjs')

let changeMyMail = (req, res) => {
    const user_id = req.user_id;
    let abort = false;

    let neueMail, pass;
    try {
        neueMail = req.body.mail;
        pass = req.body.pass;

        if (!validateEmail(neueMail)) {
            abort = true;
            res.sendStatus(400);
        }
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        User.findOne({
            _id: req.user_id
        }).then(user => {
            return new Promise((resolve, reject) => {
                bcrypt.compare(pass, user.password, (err, res) => {
                    if (res) resolve(user);
                    else reject();
                })
            })
        }).then(() => {
            User.findOne({
                _id: user_id
            }).then(doc => {
                doc.mail = neueMail;
                doc.save();
                res.sendStatus(200);
            }).catch(() => {
                res.sendStatus(400);
            });
        }).catch(() => {
            res.sendStatus(403);
        })
    }
}

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

let changeMyPass = async (req, res) => {
    const user_id = req.user_id;
    let abort = false;

    let altesPass, neuesPass;
    let isPass = false;
    try {
        altesPass = req.body.pass;
        neuesPass = req.body.newpass;
        if (!neuesPass || altesPass === neuesPass) {
            res.sendStatus(400);
            return;
        }

        await User.findOne({
            _id: user_id
        }).then(async u => {
            await User.findByCredentials(u.mail, altesPass)
                .then(() => {
                    isPass = true;
                }).catch(() => {
                    isPass = false;
                })
        }).catch(() => {
            isPass = false;
        })

        if (!isPass) {
            res.sendStatus(403);
            return;
        }
    } catch (_) {
        res.sendStatus(400);
        return;
    }

    if (!abort) {
        User.findOne({
            _id: user_id
        }).then(doc => {
            if (isPass) {
                doc.password = neuesPass;
                doc.save();
                res.sendStatus(200);
            } else {
                throw 'invalid';
            }
        }).catch(() => {
            res.sendStatus(400);
        })
    }
}

let getSessions = (req, res) => {
    const user_id = req.user_id;

    User.findOne({
        _id: user_id
    }).then(u => {
        res.status(200).send(u.sessions);
    }).catch(() => {
        res.sendStatus(400);
    });
}

let logoutSession = (req, res) => {
    const user_id = req.user_id;
    let abort = false;

    let session_id;
    try {
        session_id = req.body.sessionID;
        if (!session_id) {
            abort = true;
            res.sendStatus(400);
        }
    } catch (_) {
        abort = true;
        res.sendStatus(400);
    }

    if (!abort) {
        User.findOneAndUpdate({
            _id: req.user_id
        }, {
            $pull: {'sessions': {'_id': session_id}}
        }).then(() => {
            res.sendStatus(200);
        }).catch(() => {
            res.sendStatus(400);
        })
    }
}

module.exports = {
    changeMyMail,
    changeMyPass,
    getSessions,
    logoutSession
}

FROM node:15.9-alpine3.10
RUN apk update \
    && apk add git \
    && npm install -g npm@7.5.4
RUN cd /tmp \
    && git clone https://gitlab.com/die_starken_vier/jobby.git
RUN npm i -g @angular/cli \
    && cd /tmp/jobby/Frontend/Jobportal \
    && npm i
RUN cd /tmp/jobby/Frontend/Jobportal \
    && ng build --prod
RUN cp -r /tmp/jobby/Frontend/Jobportal/dist/Jobportal/* /tmp/jobby/Backend/public/
RUN cd /tmp/jobby/Backend \
    && npm i
RUN mkdir /jobby \
    && cp -r /tmp/jobby/Backend/* /jobby \
    && cd /jobby
RUN rm -rf /tmp/jobby
WORKDIR /jobby
EXPOSE 3000
CMD ["node", "api.js"]
